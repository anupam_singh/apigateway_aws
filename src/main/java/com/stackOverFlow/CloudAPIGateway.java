package com.stackOverFlow;

import io.vertx.core.Future;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.SessionHandler;
import io.vertx.ext.web.sstore.LocalSessionStore;

public class CloudAPIGateway extends DefaultAPIGateway{

	@Override
	protected void routeRequests(Router router) {
		router.route("/api/v1/users/*").handler(rctx -> {
			dispatchRequests(rctx,"104.154.248.9");
		});
		
		router.route("/api/v2/users/*").handler(rctx -> {
			dispatchRequests(rctx,"35.186.187.63");
			//dispatchRequests(rctx,"servicea.servicea.anupam.com");
		});
		
		router.route("/api/v1/questions/*").handler(rctx -> {
			dispatchRequests(rctx,"35.190.150.30");
		});
		
		router.route("/api/v1/chat/*").handler(rctx -> {
			System.out.println("CloudAPIGateway.routeRequests() CHAT HANDLER");
			dispatchRequests(rctx,"35.197.28.5");
		});
	}
	
	private void dispatchRequests(RoutingContext context,String host) {
		// run with circuit breaker in order to deal with failure
		circuitBreaker.execute(future -> {
			int initialOffset = 5; // length of `/api/`
			// get relative path and retrieve prefix to dispatch client
			String path = context.request().uri();
			System.out.println("CloudAPIGateway.dispatchRequests() path: "+path);
			if (path.length() <= initialOffset) {
				notFound(context);
				future.complete();
				return;
			}
			String subStr = path.substring(initialOffset);
			System.out.println("CloudAPIGateway.dispatchRequests() path.substring(initialOffset): "+subStr);
			String[] strArray = subStr.split("/");
			String prefix = strArray[0];
			System.out.println("CloudAPIGateway.dispatchRequests() prefix: "+prefix);
			initialOffset = initialOffset + prefix.length() + 1;
			// generate new relative path
//			String newPath = path.substring(initialOffset + prefix.length());
			String newPath = "/" + path.substring(initialOffset);
			System.out.println("CloudAPIGateway.dispatchRequests() newPath: "+newPath);
			doDispatch_2(context, newPath, host,future);
		
		}).setHandler(ar -> {
			if (ar.failed()) {
				badGateway(ar.cause(), context);
			}
		});
	}
	
	private void doDispatch_2(RoutingContext context, String path, String host, Future<Object> cbFuture) {
		HttpClient client = vertx.createHttpClient();

		System.out.println(
				"CloudAPIGateway.doDispatch() path: " + path + " context.request().uri())))): " + context.request().uri());
		System.out.println("CloudAPIGateway.doDispatch() Authorization header: "+context.request().getHeader("Authorization"));
		System.out.println("CloudAPIGateway.doDispatch() hostttt: "+host);
		HttpClientRequest toReq = client.request(context.request().method(), 80, host, path, response -> {
			response.bodyHandler(body -> {
				System.out.println("CloudAPIGateway.doDispatch() response: "+response);
				System.out.println("CloudAPIGateway.doDispatch() response.statusCode(): " + response.statusCode());
				if (response.statusCode() >= 500) { // api endpoint server
													// error, circuit breaker
													// should fail
					cbFuture.fail(response.statusCode() + ": " + body.toString());
				} else {
					HttpServerResponse toRsp = context.response().setStatusCode(response.statusCode());
					response.headers().forEach(header -> {
						System.out.println("CloudAPIGateway.doDispatch() header.getKey(): "+header.getKey());
						System.out.println("CloudAPIGateway.doDispatch() header.getValue(): "+header.getValue());
						toRsp.putHeader(header.getKey(), header.getValue());
					});
//					System.out.println("CloudAPIGateway.doDispatch() body: "+body);
					System.out.println("CloudAPIGateway.doDispatch() body.toString(): "+body.toString());
					// send response
					toRsp.end(body);
					cbFuture.complete();
				}
			});
		});
		// set headers
		context.request().headers().forEach(header -> {
			toReq.putHeader(header.getKey(), header.getValue());
		});
		if (context.user() != null) {
			toReq.putHeader("user-principal", context.user().principal().encode());
		}
		System.out.println("CloudAPIGateway.doDispatch() context.getBody(): "+context.getBody());
		// send request
		if (context.getBody() == null) {
			toReq.end();
		} else {
			toReq.end(context.getBody());
		}
	
	}

	@Override
	protected void createHttpServer(final Router router,Future<Void> future) {
		vertx.createHttpServer().requestHandler(router::accept).listen(DEFAULT_PORT);
	}

	@Override
	protected void createSessionHandler(Router router) {
		router.route().handler(SessionHandler.create(LocalSessionStore.create(vertx)));
	}

}
