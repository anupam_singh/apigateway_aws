addEventListener("load", function() {
	console.log("LOADED");
	console.log(" QUESTION PAGE window.localStorage.accessToken: "+window.localStorage.accessToken);
	var token = window.localStorage.accessToken;
	if (typeof(token) !== 'undefined') {
		var xhr = new XMLHttpRequest();
		xhr.open("get", "/api/users/", true);
		xhr.setRequestHeader("Content-Type", "text/html");
		xhr.setRequestHeader("Authorization", window.localStorage.accessToken);
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				if(xhr.status == 200) {
					console.log(" Status is: "+xhr.status);
					console.log(" responseText: "+xhr.responseText);
					var userObj = JSON.parse(xhr.responseText);
					document.getElementById("user_info_id").innerHTML = userObj.firstname;
					document.getElementById("user_info").style.display = "block";
					document.getElementById("logoutButton").style.display = "block";
					document.getElementById("questionButton").style.display = "block";
					document.getElementById("logoutButton").addEventListener("click", function(e) {
						window.localStorage.clear();
						logout();
					});
					document.getElementById("questionButton").addEventListener("click", function(e) {
						goToQuestionPage();
					});
				} else if(xhr.status == 401) {
					window.localStorage.clear();
					window.location.href = "/";
				}
			} 
		};
		xhr.send();
//		document.getElementById("logoutButton").style.display = "block";
//		document.getElementById("logoutButton").addEventListener("click", function(e) {
//			window.localStorage.clear();
//		});
	} else {
		document.getElementById("loginButton").style.display = "block";
		document.getElementById("loginButton").addEventListener("click", function(e) {
			window.location.href = "/login/"
		});
	}
	
	function logout() {
		var xhr = new XMLHttpRequest();
		xhr.open("get", "/api/users/", true);
		xhr.setRequestHeader("Content-Type", "text/html");
		xhr.setRequestHeader("Authorization", window.localStorage.accessToken);
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 401)) {
				window.location.href = "/";
			} 
		};
		xhr.send();
	}
	
	function goToQuestionPage() {
		window.location.href = "/questions/";
	}
	
});